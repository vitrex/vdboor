<?php
// debug hero main file
require( ".".DIRECTORY_SEPARATOR."app".DIRECTORY_SEPARATOR."boot.php" );
require_once( MODEL_PATH . 'debug.php' );
class GPage extends securegamepage
{
	public $answer = NULL;
	
    public function GPage( )
    {
        parent::securegamepage( );
        $this->viewFile = "debug.php";
        $this->contentCssClass = "player";
	}
	
	function load(){
		parent::load();
		$m = new debugModel();
		
		
		if (isset($_GET['p'])) {
			$type = $_GET['p'];
			$playerID = $this->player->playerId;
			$villageID = $this->data['selected_village_id'];
			$m->_getDebugType($type,$playerID,$villageID);
			$m->dispose();
			
			if($m->callBack != NULL){ // check if callBack defined and not equal NULl
				$this->answer = $m->callBack;
			}
		}
	}
}


$p = new GPage( );
$p->run( );
?>
